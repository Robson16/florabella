<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <header id="header">        
        <nav id="navbar" class="navbar container">
            <?php
            if (function_exists('the_custom_logo') && has_custom_logo()) {
                the_custom_logo();
            } else {
                echo '<h1>' . get_bloginfo('title') . '</h1>';
            }
            ?>

            <button type="button" class="navbar-toggler" data-target="#navbar-nav">
                <span class="navbar-toggler-icon"></span>             
            </button>

            <?php
            wp_nav_menu( array(
                'theme_location' => 'main_menu',
                'container' => 'div',
                'container_class' => 'navbar-collapse',
                'container_id' => 'navbar-nav',
                'menu_class' => 'navbar-nav has-sub-menu-to-left',
            ) );
            ?>
        </nav>        
    </header>