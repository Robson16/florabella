<?php

/**
 * Functions and Definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Enqueuing of scripts and styles.
 */
function florabella_scripts() {
    wp_enqueue_style( 'florabella-reset-styles', get_template_directory_uri() . '/assets/css/shared/reset.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'florabella-gutenberg-styles', get_template_directory_uri() . '/assets/css/shared/gutenberg.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'florabella-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/styles.css', array(), wp_get_theme()->get( 'Version' ) );    

    wp_enqueue_script( 'florabella-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-scripts.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ), true );
}

add_action('wp_enqueue_scripts', 'florabella_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function florabella_gutenberg_scripts() {
    wp_enqueue_script('florabella-editor-script', get_stylesheet_directory_uri() . '/assets/js/admin/editor.js', array('wp-blocks', 'wp-dom'), '1.0', true);
}

add_action('enqueue_block_editor_assets', 'florabella_gutenberg_scripts');


/** 
 * Set theme defaults and register support for various WordPress features.
 */
function florabella_setup() {
    // Enabling translation support
    $textdomain = 'florabella';
    load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
    load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

    // Customizable logo
    add_theme_support( 'custom-logo', array(
        'height'      => 52,
        'width'       => 350,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

    // Menu registration
    register_nav_menus(array(
        'main_menu' => __('Main Menu', 'florabella'),
    ));

    // Let WordPress manage the document title.
    add_theme_support('title-tag');
    
    // Enable support for featured image on posts and pages.		 	
    add_theme_support( 'post-thumbnails' );

    // Load custom styles in the editor.
    add_theme_support( 'editor-styles' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/reset.css' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/gutenberg.css' );

    // Enable support for embedded media for full weight
    add_theme_support( 'responsive-embeds' );

    // Enables wide and full dimensions
    add_theme_support( 'align-wide' );

    // Standard style for each block.
	add_theme_support( 'wp-block-styles' );

    // Disable custom colors
    add_theme_support( 'disable-custom-colors' );

    // Creates the specific color palette
    add_theme_support('editor-color-palette', array(
        array(
            'name'  => __('White', 'florabella'),
            'slug'  => 'white',
            'color'    => '#ffffff',
        ),
        array(
            'name'  => __('Black', 'florabella'),
            'slug'  => 'black',
            'color'    => '#000000',
        ),
        array(
            'name'  => __('Mandarin', 'florabella'),
            'slug'  => 'mandarin',
            'color'    => '#ef773b',
        ),
        array(
            'name'  => __('Maximum Green Yellow', 'florabella'),
            'slug'  => 'maximum-green-yellow',
            'color'    => '#dde164',
        ),
        array(
            'name'  => __('Nadeshiko Pink', 'florabella'),
            'slug'  => 'nadeshiko-pink',
            'color'    => '#f3a7c0',
        ),
        array(
            'name'  => __('Medium Turquoise', 'florabella'),
            'slug'  => 'medium-turquoise',
            'color'    => '#73c5c3',
        ),
        array(
            'name'  => __('Shadow Blue', 'florabella'),
            'slug'  => 'shadow-blue',
            'color'    => '#7c91ae',
        ),
    ));

    // Disable custom gradients
    add_theme_support( 'disable-custom-gradients' );
}

add_action( 'after_setup_theme', 'florabella_setup' );

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function florabella_sidebars() {
    // Args used in all calls register_sidebar().
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );
    
    // Footer #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Footer #1', 'florabella'),
        'id' => 'florabella-sidebar-footer-1',
        'description' => __('The widgets in this area will be displayed in the first column in Footer.', 'florabella'),
    ) ) );

    // Footer #2
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Footer #2', 'florabella'),
        'id' => 'florabella-sidebar-footer-2',
        'description' => __('The widgets in this area will be displayed in the second column in Footer.', 'florabella'),
    ) ) );

    // Footer #3
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Footer #3', 'florabella'),
        'id' => 'florabella-sidebar-footer-3',
        'description' => __('The widgets in this area will be displayed in the third column in Footer.', 'florabella'),
    )));

    // Footer #4
    register_sidebar(array_merge($shared_args, array(
        'name' => __('Footer #4', 'florabella'),
        'id' => 'florabella-sidebar-footer-4',
        'description' => __('The widgets in this area will be displayed in the fourth column in Footer.', 'florabella'),
    )));
}

add_action( 'widgets_init', 'florabella_sidebars' );
