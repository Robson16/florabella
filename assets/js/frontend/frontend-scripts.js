/**
 * Front-End JS
 */

jQuery(document).ready(function ($) {
    $('.navbar-toggler').click(function () {
        let target = $(this).data('target');
        $(target).toggleClass('show');
    });

    $('.menu-item-has-children > a').click(function (event) {
        event.preventDefault();
    })
});