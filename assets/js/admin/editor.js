wp.domReady(() => {
    wp.blocks.registerBlockStyle('core/paragraph', {
        name: 'cursive-letter',
        label: 'Cursive Letter'
    });
    wp.blocks.registerBlockStyle('core/heading', {
        name: 'cursive-letter',
        label: 'Cursive Letter'
    });
    wp.blocks.registerBlockStyle('core/cover', {
        name: 'flower-frame-one',
        label: 'Moldura de Flor Um'
    });
    wp.blocks.registerBlockStyle('core/cover', {
        name: 'flower-frame-two',
        label: 'Moldura de Flor Dois'
    });
    wp.blocks.registerBlockStyle('core/cover', {
        name: 'flower-frame-three',
        label: 'Moldura de Flor Três'
    });
    wp.blocks.registerBlockStyle('core/cover', {
        name: 'flower-frame-four',
        label: 'Moldura de Flor Quatro'
    });
});